import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
} from 'react-native';

import { StackNavigator } from 'react-navigation';
import Splash from './Screens/Splash/SplashVC';
import Main from './Screens/Main/MainVC';

type Props = {};

export default class AlertAppDelegate extends Component<Props> {

    componentDidMount() {
        const self = this;
        this.createNotificationListeners();
    }

    //Remove listeners allocated in createNotificationListeners()
    componentWillUnmount() {
        this.notificationListener();
        this.notificationOpenedListener();
    }

    async createNotificationListeners() {
        /*
        * Triggered when a particular notification has been received in foreground
        * */
    }

    state = {
        key: 100
    };

    reload() {
        this.setState({
            key: this.state.key + 1
        });
    };

    render() {
        return (
            <RootStack style={styles.container}>

            </RootStack>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
});

const RootStack = StackNavigator(
    {
        Splash: {
            screen: Splash,
        },
        Main: {
            screen: Main,
        },
    },
    {
        initialRouteName: 'Splash',
    }
);

AppRegistry.registerComponent('AlertAppDelegate', () => AlertAppDelegate);