
import React, {Component} from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';

export default class Splash extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.appname}>アラート受信アプリ</Text>
                <Text style={styles.detail}>{'デバイスID' + 'サーバ検証完了済'}</Text>
                <Button
                    style={styles.button}
                    title="Go to Main..."
                    onPress={ () => {
                            this.props.navigation.push("MainVC");
                        }
                    }
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3c9c69',
    },
    appname: {
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 0,
    },
    detail: {
        fontSize: 20,
        textAlign: 'center',
        color: '#333333',
        margin: 30,
    },
    button: {
        fontSize: 20,
        textAlign: 'center',
        color: '#8bbb8e',
        backgroundColor: '#ffffff',
        margin: 0,
    }
});

