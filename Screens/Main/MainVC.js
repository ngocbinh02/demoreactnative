import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Logo from './Logo';
import ButtonSubmit from './ButtonSubmit';

export default class Main extends Component {
    render() {
        return (
            <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                <Logo />
                <ButtonSubmit controller={this} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    appname: {
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 0,
    },
    detail: {
        fontSize: 20,
        textAlign: 'center',
        color: '#333333',
        margin: 30,
    },
});