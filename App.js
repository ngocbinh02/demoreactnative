import React from 'react';
import { View, Text, Button, AppRegistry } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SplashVC from "./Screens/Splash/SplashVC";
import MainVC from "./Screens/Main/MainVC";
import TableViewVC from "./Screens/Controls/TableViewController";

const AppNavigator = createStackNavigator(
    {
        SplashVC: { screen: SplashVC, appDelegate: AppContainer },
        MainVC: MainVC,
        TableViewVC: TableViewVC
    },
    {
        initialRouteName: 'SplashVC',
    }
);

const AppContainer = createAppContainer(AppNavigator);
export default class App extends React.Component {
    render() {
        return <AppContainer />;
    }
}
AppRegistry.registerComponent('AppDelegate', () => App);